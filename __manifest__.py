{
    "name": "CG SCOP - Ressources",
    "summary": "Gestion des ressources pour la CG Scop",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "license": "AGPL-3",
    "application": False,
    "depends": [
        'calendar',
        'cgscop_partner',
    ],
    "data": [
        "security/security_rules.xml",
        "security/ir.model.access.csv",
        "views/scop_resource.xml",
    ],
    'installable': True,
    'auto_install': False,
}
