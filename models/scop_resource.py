# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class ScopResource(models.Model):
    _name = "scop.resource"
    _description = "SCOP Resource"

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    active = fields.Boolean(default=True)
    name = fields.Char("Nom de la ressource", required=True)
    resource_type = fields.Selection([
        ('room', 'Salle'),
        ('car', 'Véhicule'),
        ('video', 'Vidéo-projecteur'),
        ('other', 'Autre')],
        string="Type de ressource")
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)
