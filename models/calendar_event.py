# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class ScopResourceEvent(models.Model):
    _inherit = 'calendar.event'

    scop_resource_id = fields.Many2one('scop.resource', "Ressource")

    @api.multi
    @api.constrains('scop_resource_id', 'start', 'stop')
    def _check_resource_already_used(self):
        for record in self:
            if record.scop_resource_id:
                resource_check = self.env['calendar.event'].search([
                    ('id', '!=', record.id),
                    ('start', '<', record.stop),
                    ('stop', '>', record.start),
                    ('scop_resource_id', '=', record.scop_resource_id.id)
                ])
                if resource_check:
                    raise ValidationError(
                        _(
                            "La ressource %s n'est pas disponible",
                        )
                        % record.scop_resource_id.name,
                    )

    # ------------------------------------------------------
    # Onchange Fields
    # ------------------------------------------------------
    @api.onchange('scop_resource_id')
    def onchange_scop_resource_id(self):
        for event in self:
            if event.scop_resource_id:
                event.name = "Réservation : " + event.scop_resource_id.name